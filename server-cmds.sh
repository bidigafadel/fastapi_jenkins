#!/usr/bin/env bash

export DATABASE_HOSTNAME=postgres_fastapi
export DATABASE_NAME=fastapi
export DATABASE_PORT=5432
export DATABASE_PASSWORD=password
export DATABASE_USERNAME=postgres
export SECRET_KEY=uglgeda68769BK29879233297320jgvjkglge879879GBIK
export ALGORITHM=HS256
export ACCESS_TOKEN_EXPIRE_MINUTES=30

export IMAGE=$1
docker-compose -f docker-compose.yml up --detach
echo "success"
